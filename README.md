Introduction
===

One-time passwords (OTP) are commonly used as two-/multi-factor authentication (2FA/MFA)
for increased account security by requiring the usual username and password, and also a
one-time password which changes frequently. In this app, a time-based OTP (TOTP) is shown.
The TOTP changes every 30 seconds by default, but can be customized in the settings tab.


Motivation
===

When there is only a few seconds left before the TOTP changes,
I have observed that people wait for it to expire and change
before typing it into the requestor's page. This wastes valuable time.

To avoid the described problem, this app shows the current TOTP _and_
the upcoming TOTP to prevent people from waiting idly for the new TOTP to appear.

Although some systems have a tolerance of one period
(that is, even if you used a recently-expired OTP, it would still work),
it is not commonly known.
This app also allows people who are not able to memorize the whole OTP
within the last two or three seconds from having to wait.


Screenshots
===

| Main TOTP tab                                                                                                                | Settings tab                                  |
| ---------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- |
| [![low time remaining][img-thumb-time-high]][img-time-high] [![high time remaining][img-thumb-time-low]][img-time-low] | [![][img-thumb-settings]][img-settings] |


[img-thumb-time-high]: images/screenshots/thumbnails/main-general.png
[img-thumb-time-low]: images/screenshots/thumbnails/main-urgent.png
[img-thumb-settings]: images/screenshots/thumbnails/settings.png

[img-time-high]: images/screenshots/main-general.png
[img-time-low]: images/screenshots/main-urgent.png
[img-settings]: images/screenshots/settings.png
