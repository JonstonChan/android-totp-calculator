default:
  image: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH

variables:
  # Default manual pipeline variables
  DOCKER_REBUILD:
    value: "false"
    description: "Set to 'true' to rebuild Docker image"

# Normally, these jobs should be run on every commit or pre-merge and post-merge
# However, to save on compute resources (cost), these have been set to manually run
# and all tasks (build, lint, test) have been put into a single job to avoid overhead
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: always
    - when: never

stages:
  - prepare
  - build

before_script:
  - chmod +x ./gradlew

docker image:
  stage: prepare
  image: docker:20.10.17
  services:
    - docker:20.10.17-dind
  script:
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    - docker build -t "$CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH" .
    - docker push "$CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH"
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
      changes:
        - Dockerfile
      when: manual
    - if: ($CI_PIPELINE_SOURCE == "web") && ($DOCKER_REBUILD == "true")
      when: always

all:
  stage: build
  timeout: 15 minutes
  variables:
    GRADLE_OPTIONS: --console=plain --build-cache --gradle-user-home cache/.gradle/
  script:
    # Version checks
    - ./gradlew $GRADLE_OPTIONS --version

    # Lint stage
    - ./gradlew $GRADLE_OPTIONS :app:lintDebug -PbuildDir=lint

    # Assemble stage
    - ./gradlew $GRADLE_OPTIONS :app:assembleDebug
    - ./gradlew $GRADLE_OPTIONS :app:assembleDebugMinified

    - ./gradlew $GRADLE_OPTIONS :app:assembleRelease

    # debug.keystore generated after debug build
    # Warning: a debug key has been used for demo purposes only
    - old_working_dir="$PWD"
    - cd "app/build/outputs/apk/release"
    - cp "app-release-unsigned.apk" "app-release-signed-debug-key.apk"
    - jarsigner
        -verbose
        -keystore "$HOME/.android/debug.keystore"
        -storepass android
        -keypass android
        "app-release-signed-debug-key.apk"
        androiddebugkey
    - zipalign -v 4 "app-release-signed-debug-key.apk" "app-release-signed-debug-key-aligned.apk"
    - rm "app-release-signed-debug-key.apk"
    - cd "$old_working_dir"

    # Test stage
    - ./gradlew $GRADLE_OPTIONS :app:testDebugUnitTest
  artifacts:
    when: always
    paths:
    - app/build/outputs/
    - app/build/reports/tests/
    - app/lint/reports/
  cache:
    key:
      prefix: $CI_COMMIT_REF_NAME
      files:
        - gradle/wrapper/gradle-wrapper.properties
    paths:
      - cache/.gradle/caches/
      - cache/.gradle/wrapper/
