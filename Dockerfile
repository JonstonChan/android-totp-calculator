FROM eclipse-temurin:18.0.2_9-jdk

# Taken from ./app/build.gradle : compileSdk
ARG ANDROID_COMPILE_SDK=32

# Taken from https://developer.android.com/studio/releases/build-tools
ARG ANDROID_BUILD_TOOLS=30.0.3

# Get latest number from https://developer.android.com/studio/index.html#command-tools,
# including URL to download tools below
ARG ANDROID_SDK_TOOLS=8512546

ENV ANDROID_HOME=$PWD/android-sdk-linux
ENV PATH="$PATH:$PWD/android-sdk-linux/platform-tools/"

# Note: manually added "latest" in "cmdline-tools/latest/bin/sdkmanager"
# This fixes the "Could not determine SDK root"
ARG ANDROID_SDK_MANAGER="$PWD/android-sdk-linux/cmdline-tools/latest/bin/sdkmanager"

RUN apt-get --quiet update --yes && \
	apt-get --quiet install --yes lib32stdc++6 lib32z1 unzip wget zipalign && \
	wget --quiet --output-document=android-sdk.zip "https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip" && \
	unzip -d "android-sdk-linux" "android-sdk.zip" && \
	rm "android-sdk.zip" && \
	cd "android-sdk-linux" && \
	mkdir "cmdline-tools-top" && \
	mv "cmdline-tools" "cmdline-tools-top/latest" && \
	mv "cmdline-tools-top" "cmdline-tools" && \
	echo y | "$ANDROID_SDK_MANAGER" "platforms;android-${ANDROID_COMPILE_SDK}" && \
	echo y | "$ANDROID_SDK_MANAGER" "platform-tools" && \
	echo y | "$ANDROID_SDK_MANAGER" "build-tools;${ANDROID_BUILD_TOOLS}" && \
	# disable checking for EPIPE error and use yes to accept all licenses && \
	yes 2>/dev/null | "$ANDROID_SDK_MANAGER" --licenses && \
	jarsigner -version && \
	apt-get clean
