package com.jc.totpcalculator.library.main

import org.apache.commons.codec.binary.Base32
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import kotlin.experimental.and
import kotlin.math.pow


fun Long.toEightByteArray(): ByteArray {
    return (7 downTo 0).map { ((this shr (it * 8)) and 0xFF).toByte() }.toByteArray()
}

fun Int.toOtp(digits: Int): Int = this % (10.toDouble().pow(digits).toInt())


class Totp(
    keyString: String,
    totpTimeLength: Int,
    numOtpDigits: Int,
    otpAlgorithm: String,
    counterOffset: Int,
) {
    private var key: ByteArray
    private var totpTimeLength: Int
    private var numOtpDigits: Int
    private var otpAlgorithm: String
    private var counterOffset: Int

    init {
        val keyBytes = Base32().decode(keyString)
        this.key = keyBytes

        this.totpTimeLength = totpTimeLength
        this.numOtpDigits = numOtpDigits
        this.otpAlgorithm = otpAlgorithm
        this.counterOffset = counterOffset
    }

    fun getSecondsRemaining(): Long {
        val secondsSinceEpoch = System.currentTimeMillis() / 1000

        return totpTimeLength - secondsSinceEpoch % totpTimeLength
    }

    fun getToken(): String {
        val mac = Mac.getInstance(this.otpAlgorithm)

        val key = SecretKeySpec(this.key, this.otpAlgorithm)
        mac.init(key)

        val secondsSinceEpoch = System.currentTimeMillis() / 1000
        val otpCounter = (secondsSinceEpoch / this.totpTimeLength) + this.counterOffset

        val msg = otpCounter.toEightByteArray()
        val hmacResult = mac.doFinal(msg)

        val offset = (hmacResult[19] and 0x0F).toInt()

        val binaryCode =
            ((hmacResult[offset + 0].toInt() and 0x7F) shl 24)
                .or((hmacResult[offset + 1].toInt() and 0xFF) shl 16)
                .or((hmacResult[offset + 2].toInt() and 0xFF) shl 8)
                .or((hmacResult[offset + 3].toInt() and 0xFF))

        val otpCode = binaryCode.toOtp(this.numOtpDigits)

        return formatOtpCode(otpCode.toString())
    }

    private fun chunkOtpCode(otpCode: String): String {
        val otpCodeChunked = when (val it = otpCode.length) {
            in 0..5 -> otpCode
            in 6..8 -> {
                val spaceLocation = it / 2

                val firstPart = otpCode.substring(0, spaceLocation)
                val secondPart = otpCode.substring(spaceLocation)

                return "$firstPart $secondPart"
            }
            9, 10 -> {
                val spaceLocation1 = it / 3
                val spaceLocation2 = spaceLocation1 + 3

                val firstPart = otpCode.substring(0, spaceLocation1)
                val secondPart = otpCode.substring(spaceLocation1, spaceLocation2)
                val thirdPart = otpCode.substring(spaceLocation2)

                return "$firstPart $secondPart $thirdPart"
            }
            else -> otpCode
        }

        return otpCodeChunked
    }

    fun formatOtpCode(otpCode: String): String {
        val otpCodePadded = otpCode.padStart(this.numOtpDigits, '0')

        return chunkOtpCode(otpCodePadded)
    }
}
