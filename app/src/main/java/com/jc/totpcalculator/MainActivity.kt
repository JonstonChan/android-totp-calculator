package com.jc.totpcalculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.jc.totpcalculator.databinding.ActivityMainBinding
import com.jc.totpcalculator.ui.main.MainPlaceholderFragment
import com.jc.totpcalculator.ui.main.SectionsPagerAdapter
import com.jc.totpcalculator.ui.main.SettingsFragment
import com.jc.totpcalculator.ui.main.Tab
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val tabData = listOf(
            Tab(R.string.tab_text_main, MainPlaceholderFragment.newInstance()),
            Tab(R.string.tab_text_settings, SettingsFragment.newInstance()),
        )

        val viewPager: ViewPager2 = binding.mainViewPager
        viewPager.adapter = SectionsPagerAdapter(this, tabData)

        val tabLayout: TabLayout = binding.mainTabLayout
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = this.resources.getString(tabData[position].titleId)
        }.attach()
    }
}
