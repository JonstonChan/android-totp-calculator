package com.jc.totpcalculator.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

data class Tab(val titleId: Int, val fragment: Fragment)

data class SectionsPagerAdapter(
    private val fragmentActivity: FragmentActivity,
    private val tabData: List<Tab>
) :
    FragmentStateAdapter(fragmentActivity) {
    override fun createFragment(position: Int): Fragment = tabData[position].fragment
    override fun getItemCount(): Int = tabData.size
}
