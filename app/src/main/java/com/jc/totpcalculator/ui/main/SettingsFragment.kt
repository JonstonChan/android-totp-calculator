package com.jc.totpcalculator.ui.main

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.jc.totpcalculator.R
import com.jc.totpcalculator.databinding.FragmentSettingsBinding


// From https://stackoverflow.com/a/51799663
fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

class SettingsFragment : Fragment() {
    private lateinit var viewModel: SettingsFragmentViewModel
    private var _binding: FragmentSettingsBinding? = null

    private val binding get() = _binding!!

    private lateinit var sharedPreference: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[SettingsFragmentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)

        sharedPreference = binding.root.context.getSharedPreferences(
            getString(R.string.saved_settings_id),
            Context.MODE_PRIVATE,
        )

        viewModel.key.observe(viewLifecycleOwner) {
            val textEditValue = binding.otpKeyInput.text.toString()
            val viewModelValue = viewModel.key.value!!

            if (textEditValue != viewModelValue) {
                binding.otpKeyInput.text = viewModelValue.toEditable()
            }
        }

        viewModel.codeLength.observe(viewLifecycleOwner) {
            val textEditValue = binding.otpCodeLengthInput.text.toString()
            val viewModelValue = viewModel.codeLength.value!!

            if (textEditValue != viewModelValue) {
                binding.otpCodeLengthInput.text = viewModelValue.toEditable()
            }
        }

        viewModel.timeLength.observe(viewLifecycleOwner) {
            val textEditValue = binding.otpTimeLengthInput.text.toString()
            val viewModelValue = viewModel.timeLength.value!!

            if (textEditValue != viewModelValue) {
                binding.otpTimeLengthInput.text = viewModelValue.toEditable()
            }
        }

        loadSettings()

        binding.otpKeyInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.key.value = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.otpCodeLengthInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.codeLength.value = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.otpTimeLengthInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.timeLength.value = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.settingsSaveButton.setOnClickListener {
            saveSettings()
        }

        return binding.root
    }

    private fun loadSettings() {
        if (!(viewModel.initialized.value!!)) {
            viewModel.initialized.value = true

            viewModel.key.value =
                sharedPreference.getString(
                    getString(R.string.saved_settings_id_key),
                    getString(R.string.settings_otp_key_default_value),
                )

            viewModel.codeLength.value =
                sharedPreference.getInt(
                    getString(R.string.saved_settings_id_otp_code_length),
                    getString(R.string.settings_otp_code_length_default_value).toInt(),
                ).toString()

            viewModel.timeLength.value =
                sharedPreference.getInt(
                    getString(R.string.saved_settings_id_otp_time_length),
                    getString(R.string.settings_otp_time_length_default_value).toInt(),
                ).toString()
        } else {
            binding.otpKeyInput.text = viewModel.key.value!!.toEditable()
            binding.otpCodeLengthInput.text = viewModel.codeLength.value!!.toEditable()
            binding.otpTimeLengthInput.text = viewModel.timeLength.value!!.toEditable()
        }
    }

    private fun saveSettings() {
        val codeLength: Int
        val timeLength: Int

        try {
            codeLength = viewModel.codeLength.value!!.toInt()
            timeLength = viewModel.timeLength.value!!.toInt()
        } catch (e: NumberFormatException) {
            Snackbar.make(
                binding.root,
                getString(R.string.settings_save_failure),
                Snackbar.LENGTH_SHORT
            ).show()

            // If any input is invalid, do not save
            return
        }

        val editor = sharedPreference.edit()
        editor.putString(
            getString(R.string.saved_settings_id_key),
            viewModel.key.value,
        )

        editor.putInt(
            getString(R.string.saved_settings_id_otp_code_length),
            codeLength,
        )

        editor.putInt(
            getString(R.string.saved_settings_id_otp_time_length),
            timeLength,
        )

        editor.apply()

        Snackbar.make(
            binding.root,
            getString(R.string.settings_save_success),
            Snackbar.LENGTH_SHORT
        ).show()
    }

    companion object {
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
