package com.jc.totpcalculator.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SettingsFragmentViewModel : ViewModel() {
    val initialized: MutableLiveData<Boolean> = MutableLiveData(false)

    val key: MutableLiveData<String> = MutableLiveData("")
    val codeLength: MutableLiveData<String> = MutableLiveData("")
    val timeLength: MutableLiveData<String> = MutableLiveData("")
}
