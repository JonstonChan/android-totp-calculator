package com.jc.totpcalculator.ui.main

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.jc.totpcalculator.R
import com.jc.totpcalculator.databinding.FragmentMainBinding
import com.jc.totpcalculator.library.main.Totp


class MainPlaceholderFragment : Fragment() {
    private lateinit var viewModel: MainFragmentViewModel
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPreference: SharedPreferences

    private val handler = Handler(Looper.getMainLooper())
    private val runnable: Runnable = object : Runnable {
        override fun run() {
            val key = sharedPreference.getString(
                getString(R.string.saved_settings_id_key),
                getString(R.string.settings_otp_key_default_value)
            )!!

            val totpTimeLength = sharedPreference.getInt(
                getString(R.string.saved_settings_id_otp_time_length),
                getString(R.string.settings_otp_time_length_default_value).toInt()
            )

            val numOtpDigits = sharedPreference.getInt(
                getString(R.string.saved_settings_id_otp_code_length),
                getString(R.string.settings_otp_code_length_default_value).toInt()
            )

            val totp = Totp(key, totpTimeLength, numOtpDigits, "HmacSHA1", 0)
            val totpNext = Totp(key, totpTimeLength, numOtpDigits, "HmacSHA1", 1)

            viewModel.otpCode.value = totp.getToken()
            viewModel.otpCodeNext.value = totpNext.getToken()

            val secondsRemaining = totp.getSecondsRemaining()
            viewModel.secondsRemaining.value = secondsRemaining

            val progress = (secondsRemaining * 100 / totpTimeLength).toInt()

            binding.countdownProgressIndicator.setProgress(progress, true)

            val progressColor: Int =
                if (secondsRemaining <= 5) {
                    binding.root.context.getColor(R.color.time_remaining_low)
                } else {
                    binding.root.context.getColor(R.color.time_remaining_high)
                }

            binding.countdownProgressIndicator.setIndicatorColor(progressColor)

            handler.postDelayed(this, 1000)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[MainFragmentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)

        sharedPreference = binding.root.context.getSharedPreferences(
            getString(R.string.saved_settings_id),
            Context.MODE_PRIVATE
        )

        viewModel.otpCode.observe(viewLifecycleOwner) {
            binding.otpCode.text = it
        }

        viewModel.otpCodeNext.observe(viewLifecycleOwner) {
            binding.otpCodeNext.text = it
        }

        viewModel.secondsRemaining.observe(viewLifecycleOwner) {
            binding.countdownProgressIndicatorText.text = it.toString()
        }

        handler.post(runnable)

        return binding.root
    }

    companion object {
        fun newInstance(): MainPlaceholderFragment {
            return MainPlaceholderFragment()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacks(runnable)
        _binding = null
    }
}
