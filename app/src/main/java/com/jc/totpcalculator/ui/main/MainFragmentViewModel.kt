package com.jc.totpcalculator.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainFragmentViewModel : ViewModel() {
    val otpCode: MutableLiveData<String> = MutableLiveData("")
    val otpCodeNext: MutableLiveData<String> = MutableLiveData("")
    val secondsRemaining: MutableLiveData<Long> = MutableLiveData(0)
}
