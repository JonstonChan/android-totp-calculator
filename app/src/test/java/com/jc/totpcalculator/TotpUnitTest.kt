package com.jc.totpcalculator

import com.jc.totpcalculator.library.main.Totp
import org.junit.Assert.assertEquals
import org.junit.Test

class TotpUnitTest {
    private val keyString = "AAAAAAAAAAAAAAAAAAAA"
    private var totpTimeLength = 30
    private var otpAlgorithm = "HmacSHA1"
    private var counterOffset = 0

    @Test
    fun test_formatOtpCode_padded() {
        val totp =
            Totp(this.keyString, this.totpTimeLength, 3, this.otpAlgorithm, this.counterOffset)

        val otpCode = "5"
        val otpCodeExpected = "005"

        assertEquals(otpCodeExpected, totp.formatOtpCode(otpCode))
    }

    @Test
    fun test_formatOtpCode_chunked() {
        val cases: List<Triple<Int, String, String>> = listOf(
            Triple(1, "5", "5"),
            Triple(2, "55", "55"),
            Triple(3, "555", "555"),
            Triple(4, "55555", "55555"),
            Triple(5, "55555", "55555"),
            Triple(6, "555555", "555 555"),
            Triple(7, "5555555", "555 5555"),
            Triple(8, "55555555", "5555 5555"),
            Triple(9, "555555555", "555 555 555"),
            Triple(10, "5555555555", "555 555 5555"),
            Triple(11, "55555555555", "55555555555"),
        )

        cases.forEach {
            val (numOtpDigits, otpCode, otpCodeExpected) = it

            val totp = Totp(
                this.keyString,
                this.totpTimeLength,
                numOtpDigits,
                this.otpAlgorithm,
                this.counterOffset
            )

            assertEquals(otpCodeExpected, totp.formatOtpCode(otpCode))
        }
    }
}
