#!/usr/bin/env bash

thumbnails_dir="thumbnails"
mkdir -p "$thumbnails_dir"

shopt -s nullglob

for image in *.jpg *.png; do
	# Based on https://www.imagemagick.org/Usage/thumbnails/#fit
	convert "$image" -thumbnail '400x400>' "$thumbnails_dir/$image"
done
